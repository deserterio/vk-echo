/**
 * VK Echo browser extension.
 * Content script injected in the https://oauth.vk.com/blank.html page.
 */
(function(){
	
	if ('URLSearchParams' in window) {
		const hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
		const params = new URLSearchParams(hash);
		const result = {
			"VK Echo": {
				access_token      : params.get('access_token'),
				expires_in        : params.get('expires_in'),
				user_id           : params.get('user_id'),
				state             : params.get('state'),
	
				error             : params.get('error'),
				error_description : params.get('error_description'),
			}
		};
		
		if (window.opener) {
			window.opener.postMessage(result, 'https://deserter.io');
			
			// Wait for response to maybe close the window.
			window.addEventListener('message', message => {
				if (message.data === "VK Echo.close") window.close();
			});
			
		} else {
			
			console.error("VK Echo: No window.opener?");
			
		}

	} else {
		
		console.error("VK Echo: browser does not support URLSearchParams. Please update.")
		
	}
	
})();
