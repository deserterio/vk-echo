# VK Echo #

Browser extension helps authorize VK Standalone Apps in browser.

Upon landing on https://oauth.vk.com/blank.html it posts a message to window.opener 
with a single field "VK Echo" containing hash parameters.

Created to get familiar with [web extensions](https://developer.mozilla.org/en-US/Add-ons/WebExtensions).

[Download and install](https://bitbucket.org/deserterio/vk-echo/downloads/vk_echo-1.0-an+fx.xpi) signed browser extension.

Tested in FireFox 62.0.3
