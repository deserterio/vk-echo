#!/bin/bash

VERSION=$1
if [ -z "$VERSION" ]; then
	echo "Please specify version number as a parameter."
	echo "in example:"
	echo "  package.sh  1.2.33"
	exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd "$DIR/src"

ZIP="$DIR/vkecho${VERSION}.zip"
zip -vr  "$ZIP"  manifest.json  vk-echo.js  icons/vk*

echo "Created $ZIP"
ls -lA "$ZIP"
